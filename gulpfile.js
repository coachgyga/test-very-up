// Gulp (https://www.npmjs.com/package/gulp)
const gulp        = require('gulp')
const sass        = require('gulp-sass')
const rename      = require('gulp-rename')
const uglify      = require('gulp-uglify')
const cleancss    = require('gulp-clean-css')
const concat      = require('gulp-concat')
const imagemin    = require('gulp-imagemin')
const notify      = require('gulp-notify')
const browsersync = require('browser-sync').create()
const del         = require('del')
const sourcemaps  = require('gulp-sourcemaps')

// PATH
const paths = {
    js: {
        src: ['node_modules/jquery/dist/jquery.min.js','./app/js/**/*.js'],
        dest: './dist/js'
    },
    css: {
        src: ['node_modules/animate.css/animate.min.css','./app/scss/**/*.scss','./app/scss/*.scss'],
        dest: './dist/css'
    },
    images: {
        src: ['./app/images/**/*'],
        dest: './dist/images'
    },
    html: {
        src: ['./*.html']
    }
}

const styles = () =>
    gulp.src(paths.css.src)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sass().on('error', sass.logError))
        .pipe(cleancss())
        .pipe(concat('styles.css'))
        .pipe(rename({ suffix: ".min" }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.css.dest))
        .pipe(browsersync.stream())
        //.pipe(notify("Css ok !"))// JS

const scripts = () =>
    gulp.src(paths.js.src)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('scripts.js'))
        .pipe(rename({ suffix: ".min" }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.js.dest))
        .pipe(browsersync.stream())
        //.pipe(notify("Js ok !"))

const images = () =>
    gulp.src(paths.images.src, {since: gulp.lastRun(images)} )
        .pipe(imagemin())
        .pipe(gulp.dest(paths.images.dest))
        //.pipe(notify("Images ok !"))

// BROWSER SYNC (live)
const browserSyncWatch = () => {
    browsersync.init({
        server: { baseDir: "./" },
        port: 3000
    })
}

const clean = () => del(['dist'])

const watchFiles = () =>
    gulp.watch(paths.js.src, scripts)
    gulp.watch(paths.css.src, styles)
    gulp.watch(paths.images.src, images)

const watcher = gulp.parallel(watchFiles, browserSyncWatch)
const build = gulp.series(clean, gulp.parallel(styles, scripts, images));

exports.watch = watcher         // exec with : npx gulp watcher
exports.default = build         // exec with : npx gulp
