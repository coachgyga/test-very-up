$(document).ready(function() {
  $(".bloc_photo").click(function() {

    if ($(this).hasClass('is-flipped')) {
      $(this).removeClass('is-flipped');
    } else {
      $(".bloc_photo").each(function(index) {
        $(this).removeClass('is-flipped');
      });
      $(this).toggleClass("is-flipped");
    }
  });
});
